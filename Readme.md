#PMBus Numeric Conversions

------------------------------

PMBus Numeric Conversions is a set of methods in C to convert floating & fixed point formats defined in the PMBus specification between a standard IEEE 32-bit floating point format.

PMBus Conversions supported:

- Linear Format (used for Timing commands)
- Unsigned VOut Linear Mode (used for VOUT_COMMAND, etc.)
- Signed VOut Linear Mode (used for VOUT_TRIM, VOUT_CAL_OFFSET)

Features:

- Conversions from float to more constrained formats offer boundary checking
  with errors or saturation warnings returned.
- Multiple interfaces to call the conversions, either with highbyte/lowbyte or 
  using a short.
- Unit tests for each conversion available.

------------------------------

## Building & Testing the conversions

Build is done with:

make clean && make 

If you want to build a 'debug' version and observe debug values in the tests, use 'make dev'


## Integrating into your project

To integrate into your project, use the compiled library along with the PMBusNumericConversions.h file. You may optionally include the ByteHelpers.h header if you need to split a short into bytes, though there's already convenience conversion methods to cover these cases.


## License

Copyright © Ericsson AB 2014

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a
compiled binary, for any purpose, commercial or non-commercial, and
by any means.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

