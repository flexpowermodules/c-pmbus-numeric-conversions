CFLAGS += -Wall -Werror -Wextra -pedantic
CFLAGS += -Wmissing-prototypes
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wmissing-declarations
# Uncomment to demo c99 parametric testing.
CFLAGS += -std=c99

OPTFLAGS+=-DDEBUG

IS_WINDOWS = false
IS_LINUX = false

#output the right binary, based on http://stackoverflow.com/questions/714100/os-detecting-makefile
ifeq ($(OS),Windows_NT)
	IS_WINDOWS = true
	CFLAGS += -D WIN32
	ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
		CFLAGS += -D AMD64
	endif
	ifeq ($(PROCESSOR_ARCHITECTURE),x86)
		CFLAGS += -D IA32
	endif
else
	CFLAGS += -fPIC
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		IS_LINUX = true
		CFLAGS += -D LINUX
		# add link to glibc library search paths (needed for powf calls to work on Linux)
		LOPTFLAGS += -lm
	endif
	ifeq ($(UNAME_S),Darwin)
		CFLAGS += -D OSX
	endif
	UNAME_P := $(shell uname -p)
	ifeq ($(UNAME_P),x86_64)
		CFLAGS += -D AMD64
	endif
	ifneq ($(filter %86,$(UNAME_P)),)
		CFLAGS += -D IA32
	endif
	ifneq ($(filter arm%,$(UNAME_P)),)
		CFLAGS += -D ARM
	endif
endif

SOURCES=$(wildcard src/**/*.c src/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))

GENERATORS_SRC=$(wildcard srcgenerators/**/*.c srcgenerators/*.c)
GENERATORS_OBJECTS=$(patsubst %.c,%.o,$(GENERATORS_SRC))
ifeq ($(OS),Windows_NT)
	GENERATORS=$(patsubst %.c,%.exe,$(GENERATORS_SRC))
else
	GENERATORS=$(patsubst %.c,%,$(GENERATORS_SRC))
endif
GENERATED_CLEANUP=$(wildcard generatedsrc/*)

# Get the list of objects based on auto-generated sources.  If this file doesn't exist, or if it is older 
# than auto.template, it will get built using the rule defined below, according to the 
# standard behavior of GNU make.  If autoobjs.mk is rebuilt, GNU make will automatically 
# restart itself after autoobjs.mk is updated.
include autoobjs.mk

TEST_SRC=$(wildcard tests/*-Test.c)
TEST_BUILDDIR=$(patsubst tests%,$(TARGETDIR)%,$(TEST_SRC))
ifeq ($(OS),Windows_NT)
	TESTS=$(patsubst %.c,%.exe,$(TEST_BUILDDIR))
else
	TESTS=$(patsubst %.c,%,$(TEST_BUILDDIR))
endif

# ifeq ($(OS),Windows_NT)
# 	TARGET=build/libPMBusNumericConversions.dll
# else
TARGETDIR=build
LIBNAME=PMBusNumericConversions
TARGET=$(TARGETDIR)/lib$(LIBNAME).a
SO_TARGET=$(patsubst %.a,%.so,$(TARGET))
# endif

# The Target Build
all: autoobjs.mk $(TARGET) $(SO_TARGET) tests

dev: CFLAGS=-g -Wall -Isrc -Wall -Wextra $(OPTFLAGS)
dev: all

# Make generated source code and generate objects list 
autoobjs.mk: CFLAGS += -c
autoobjs.mk:
	for generator in $(GENERATORS_SRC) ; do \
		if [ "$(OS)" == "Windows_NT" ]; then \
			genexe=`echo $$generator | sed "s/\.c/\\.exe/g"` ; \
		else \
			genexe=`echo $$generator | sed "s/\.c//g"` ; \
		fi ; \
		echo "compiling $$generator to $$genexe" ; \
		$(CC) $$generator -o $$genexe $(LOPTFLAGS) ; \
	done
	cd ./srcgenerators && bash generatesources.sh
	echo "AUTO_SRCS=`echo ./generatedsrc/*.c | sed "s/\.c/\\.o/g"`" > autoobjs.mk

$(TARGET): build $(AUTO_SRCS) $(OBJECTS) 
	ar rcs $@ ./generatedsrc/LinearFormatConstants.o ./generatedsrc/VOutLinearFormatConstants.o $(OBJECTS) 
	ranlib $@

$(SO_TARGET): $(TARGET) $(AUTO_SRCS) $(OBJECTS) 
	$(CC) -shared -o $@ $(AUTO_SRCS) $(OBJECTS) $(LOPTFLAGS)

build:
	@mkdir -p build
	@mkdir -p bin

# Unit Tests
.PHONY: tests
tests: 
	if ${IS_WINDOWS}; then \
		echo "Building numeric conversion tests for windows" ; \
		$(CC) $(TEST_SRC) -o $(TESTS) -L./$(TARGETDIR) -l$(LIBNAME) ; \
	elif ${IS_LINUX}; then \
		echo "Building numeric conversion tests for linux" ; \
		$(CC) $(TEST_SRC) -o $(TESTS) $(LOPTFLAGS) -Wl,-R\$$ORIGIN -L./$(TARGETDIR) -l$(LIBNAME) ; \
	else \
		echo "Building numeric conversion tests for other unix target (presumably Darwin)" ; \
		$(CC) $(TARGET) $(TEST_SRC) -o $(TESTS) ; \
	fi ; \
	cd ./build && bash ../tests/runtests.sh


	
# The Cleaner
clean:
	rm -rf build autoobjs.mk $(OBJECTS) $(TESTS) $(GENERATORS) $(GENERATED_CLEANUP)
	find . -name "*.gc*" -exec rm {} \;
	rm -rf `find . -name "*.dSYM" -print`
	rm -df generatedsrc

