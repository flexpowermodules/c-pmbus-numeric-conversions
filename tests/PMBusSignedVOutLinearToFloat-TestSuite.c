// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "../vendor/greatest/greatest.h"

#include "../src/DebugPrint.h"
#include "../src/ByteHelpers.h"
#include "../src/PMBusNumericConversions.h"

SUITE(PMBusSignedVOutLinearToFloatSuite);

TEST PMBusSignedVOutLinearToFloatZeroTest( void ) 
{
    unsigned short pmbusVOutLinearMantissa = 0x0000;
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusVOutLinearMantissa, &highByte, &lowByte);

    float convertedFloat = PMBusSignedVOutLinearToFloat( 0x00, highByte, lowByte );
    ASSERT_EQ(0.0, convertedFloat);
    PASS();
}

TEST PMBusSignedVOutLinearToFloatAllSetBitsTest( void ) 
{
    unsigned short pmbusVOutLinearMantissa = 0xFFFF;
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusVOutLinearMantissa, &highByte, &lowByte);

    float convertedFloat = PMBusSignedVOutLinearToFloat( 0xFF, highByte, lowByte );
    
    //expected result:
    // exponent of all 1's => -1
    // mantissa of all 1's => -1
    // -1 * 2^(-1) = -0.5

	DebugPrint("Converted float: %f\n", convertedFloat);
    ASSERT_EQ(-0.5, convertedFloat);
    PASS();
}


TEST PMBusSignedVOutLinearToFloatTestTwoPointFive( void ) 
{
    unsigned short mantissa = 0x5000;
    unsigned char highByte, lowByte;
    SplitShortToChars(mantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x13; //exponent commonly used in some PoL modules,
                                   //linear mode, -13 exponent

    float convertedFloat = PMBusSignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    ASSERT_EQ(2.5 , convertedFloat);
    PASS();
}

TEST PMBusSignedVOutLinearToFloatTestNegativeTwoPointFive( void ) 
{
	//2's comp of 0x5000 (2.5)
	//       b0101|0000|0000|0000
	// inv => 1010|1111|1111|1111
	// +1  => 1011|0000|0000|0000 
	//     => B000 (-2.5)

    unsigned short mantissa = 0xB000;
    unsigned char highByte, lowByte;
    SplitShortToChars(mantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x13; //exponent commonly used in some PoL modules,
                                   //linear mode, -13 exponent

    float convertedFloat = PMBusSignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    ASSERT_EQ(-2.5 , convertedFloat);
    PASS();
}

TEST PMBusSignedVOutLinearToFloatLargestPositiveValueTest( void ) 
{
	// 5 bits of exponent of    
    // 0b01111 => 15 
    //
    // 16 bits of mantissa of
    // 0b0111111111111111 => 32767
    //
    // 32767 * (2^15) => 1073709056
    //

	unsigned short mantissa = 0x7FFF;
    unsigned char highByte, lowByte;
    SplitShortToChars(mantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x0F; // linear mode, exp of 15.

    float convertedFloat = PMBusSignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    DebugPrint("Converted float: %f\n", convertedFloat);
    ASSERT_EQ(1073709056.0 , convertedFloat);
    PASS();
}

TEST PMBusSignedVOutLinearToFloatSmallestPositiveValueTest( void ) 
{
	// 5 bits of exponent of    
    // 0b10000 => -16
    //
    // 16 bits of mantissa of
    // 0b0000000000000001 => 1
    //
    // 1 * 2^-16 => 1.5259e-5
    //
    //

    unsigned short mantissa = 0x0001;
    unsigned char highByte, lowByte;
    SplitShortToChars(mantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x10; // linear mode, exp of -16.

    float convertedFloat = PMBusSignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    DebugPrint("Converted float: %f\n", convertedFloat);
    ASSERT_EQ( powf(2.0, -16) , convertedFloat);
    PASS();
}

TEST PMBusSignedVOutLinearToFloatLargestNegativeValueTest( void ) 
{
	// 5 bits of exponent of    
    // 0b01111 => 15 
    //
    // 16 bits of mantissa of
    // 0x8000 => -32768
    //
    // -32768 * (2^15) => -1073741824
    //

	unsigned short mantissa = 0x8000;
    unsigned char highByte, lowByte;
    SplitShortToChars(mantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x0F; // linear mode, exp of 15.

    float convertedFloat = PMBusSignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    DebugPrint("Converted float: %f\n", convertedFloat);
    ASSERT_EQ(-1073741824.0 , convertedFloat);
    PASS();

}

TEST PMBusSignedVOutLinearToFloatSmallestNegativeValueTest( void ) 
{
	// 5 bits of exponent of    
    // 0b10000 => -16
    //
    // 16 bits of mantissa of
    // 0xFFFF => -1
    //
    // 1 * 2^-16 => 1.5259e-5
    //
    //

    unsigned short mantissa = 0xFFFF;
    unsigned char highByte, lowByte;
    SplitShortToChars(mantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x10; // linear mode, exp of -16.

    float convertedFloat = PMBusSignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    DebugPrint("Converted float: %f\n", convertedFloat);
    ASSERT_EQ( -1.0 * powf(2.0, -16) , convertedFloat);
    PASS();
}

GREATEST_SUITE(PMBusSignedVOutLinearToFloatSuite)
{
	RUN_TEST(PMBusSignedVOutLinearToFloatZeroTest);
	RUN_TEST(PMBusSignedVOutLinearToFloatAllSetBitsTest);
	RUN_TEST(PMBusSignedVOutLinearToFloatTestTwoPointFive);
	RUN_TEST(PMBusSignedVOutLinearToFloatTestNegativeTwoPointFive);
	RUN_TEST(PMBusSignedVOutLinearToFloatLargestPositiveValueTest);
	RUN_TEST(PMBusSignedVOutLinearToFloatSmallestPositiveValueTest);
	RUN_TEST(PMBusSignedVOutLinearToFloatLargestNegativeValueTest);
	RUN_TEST(PMBusSignedVOutLinearToFloatSmallestNegativeValueTest);
}

