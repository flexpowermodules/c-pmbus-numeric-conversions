// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 #include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "../vendor/greatest/greatest.h"

#include "../src/DebugPrint.h"
#include "../src/ByteHelpers.h"
#include "../src/PMBusNumericConversions.h"

SUITE(PMBusUnsignedVOutLinearToFloatSuite);

TEST PMBusUnsignedVOutLinearToFloatZeroTest( void ) 
{
    unsigned short pmbusVOutLinearMantissa = 0x0000;
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusVOutLinearMantissa, &highByte, &lowByte);

    float convertedFloat = PMBusUnsignedVOutLinearToFloat( 0x00, highByte, lowByte );
    ASSERT_EQ(0.0, convertedFloat);
    PASS();
}

TEST PMBusUnsignedVOutLinearToFloatAllSetBitsTest( void ) 
{
    unsigned short pmbusVOutLinearMantissa = 0xFFFF;
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusVOutLinearMantissa, &highByte, &lowByte);

    float convertedFloat = PMBusUnsignedVOutLinearToFloat( 0xFF, highByte, lowByte );
    
    //expected result:
    // exponent of all 1's => -1
    // mantissa of all 1's => 65535
    // 2^(-1) * 65535 = 32767.5

    DebugPrint("Converted float: %f\n", convertedFloat);
    ASSERT_EQ(32767.5 , convertedFloat);
    PASS();
}

TEST PMBusUnsignedVOutLinearToFloatTestTwoPointFive( void ) 
{
    unsigned short pmbusVOutLinearMantissa = 0x5000;
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusVOutLinearMantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x13; //exponent commonly used in some PoL modules,
                                   //linear mode, -13 exponent

    float convertedFloat = PMBusUnsignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    ASSERT_EQ(2.5 , convertedFloat);
    PASS();
}

TEST PMBusUnsignedVOutLinearToFloatLargestValueTest( void ) 
{
    // 5 bits of exponent of    
    // 0b01111 => 15 
    //
    // 16 bits of mantissa of
    // 0b1111111111111111 => 65535
    //
    // 65535 * (2^15) => 2147450880
    //
    //

    unsigned short pmbusVOutLinearMantissa = 0xFFFF;
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusVOutLinearMantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x0F; // linear mode, exp of 15.

    float convertedFloat = PMBusUnsignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    DebugPrint("Converted float: %f\n", convertedFloat);
    ASSERT_EQ(2147450880.0 , convertedFloat);
    PASS();

}

TEST PMBusUnsignedVOutLinearToFloatSmallestValueTest( void ) 
{
	// 5 bits of exponent of    
    // 0b10000 => -16
    //
    // 16 bits of mantissa of
    // 0b0000000000000001 => 1
    //
    // 1 * 2^-16 => 1.5259e-5
    //
    //

    unsigned short pmbusVOutLinearMantissa = 0x0001;
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusVOutLinearMantissa, &highByte, &lowByte);
    unsigned char voutMode = 0x10; // linear mode, exp of -16.

    float convertedFloat = PMBusUnsignedVOutLinearToFloat( voutMode, highByte, lowByte );
    
    DebugPrint("Converted float: %f\n", convertedFloat);
    ASSERT_EQ( powf(2.0, -16) , convertedFloat);
    PASS();
}

GREATEST_SUITE(PMBusUnsignedVOutLinearToFloatSuite)
{
    RUN_TEST(PMBusUnsignedVOutLinearToFloatZeroTest);
    RUN_TEST(PMBusUnsignedVOutLinearToFloatAllSetBitsTest);
    RUN_TEST(PMBusUnsignedVOutLinearToFloatTestTwoPointFive);
    RUN_TEST(PMBusUnsignedVOutLinearToFloatLargestValueTest);
    RUN_TEST(PMBusUnsignedVOutLinearToFloatSmallestValueTest);
}
