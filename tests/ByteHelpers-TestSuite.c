// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "../vendor/greatest/greatest.h"

#include "../src/ByteHelpers.h"

SUITE(ByteHelpersSuite);

TEST SplitShortToCharsZeroTest(void)
{
	unsigned char highByte, lowByte;
	SplitShortToChars(0x0000, &highByte, &lowByte);
	ASSERT_EQ(0x00, highByte);
	ASSERT_EQ(0x00, lowByte);
	PASS();
}

TEST SplitShortToCharsMSBTest(void)
{
	unsigned char highByte, lowByte;	
	SplitShortToChars(0x1011, &highByte, &lowByte);
	ASSERT_EQ(0x10, highByte);
	ASSERT_EQ(0x11, lowByte);
	PASS();
}

TEST SplitShortToCharsAllSetBitsTest(void)
{
	unsigned char highByte, lowByte;	
	SplitShortToChars(0xFFFF, &highByte, &lowByte);
	ASSERT_EQ(0xFF, highByte);
	ASSERT_EQ(0xFF, lowByte);
	PASS();
}

GREATEST_SUITE(ByteHelpersSuite)
{
    RUN_TEST(SplitShortToCharsZeroTest);
	RUN_TEST(SplitShortToCharsMSBTest);
	RUN_TEST(SplitShortToCharsAllSetBitsTest);
}

