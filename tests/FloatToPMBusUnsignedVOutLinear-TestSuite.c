// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "../vendor/greatest/greatest.h"

#include "../src/DebugPrint.h"
#include "../src/PMBusNumericConversions.h"


SUITE(FloatToPMBusUnsignedVOutLinearSuite);

TEST ZeroFloatToPMBusUnsignedVOutLinearShouldBeZero( void )
{
	float zero = 0.0;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusUnsignedVOutLinear(zero,
											0x13,
					   						&highByte,
					   						&lowByte);

	ASSERT_EQ(highByte, 0x00);
	ASSERT_EQ( lowByte, 0x00);
	ASSERT_EQ( result, CONVERSION_OK);
	PASS();

} 

TEST ExceedingLargestPositiveFloatToPMBusUnsignedVOutLinearShouldSaturate( void ) 
{

	// highest possible for an exponent of -13
	// 65535 * 2^(-13) => 7.9999

	float exceedingLargestPositiveValue = 8.0; 
	signed char exponent = 0x13;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusUnsignedVOutLinear(exceedingLargestPositiveValue,
											exponent,
					   						&highByte,
					   						&lowByte);
	
	DebugPrint("highByte: %02X, lowByte: %02X, result: %d\n", highByte, lowByte, result);
	
	ASSERT_EQ(highByte, 0xFF);
	ASSERT_EQ( lowByte, 0xFF);
	ASSERT_EQ( result, CONVERSION_WARNING_SATURATED_LARGEST_POSITIVE_VALUE);
	PASS();


}

TEST ExceedingSmallestPositiveFloatToPMBusUnsignedVOutLinearShouldSaturate( void ) 
{

	// smallest possible for an exponent of -13
	//1 * 2^(-13) => 0.00012207031

	float exceedingSmallestPositiveValue = 0.00009; 
	signed char exponent = 0x13;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusUnsignedVOutLinear(exceedingSmallestPositiveValue,
											exponent,
					   						&highByte,
					   						&lowByte);


	DebugPrint("highByte: %02X, lowByte: %02X, result: %d\n", highByte, lowByte, result);
	
	ASSERT_EQ(highByte, 0x00);
	ASSERT_EQ( lowByte, 0x01);
	ASSERT_EQ( result, CONVERSION_WARNING_SATURATED_SMALLEST_POSITIVE_VALUE);
	PASS();


//TODO: another 'smallest' test where:
//
//  - the value is something smaller, like 0.00001, 
//  - currently this results in zero, but still reports back that the value saturated.
}


TEST NegativeFloatToPMBusUnsignedVOutLinearShouldReturnError( void )
{
	float negativeValue = -1.0; 
	signed char exponent = 0x13;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusUnsignedVOutLinear(negativeValue,
											exponent,
					   						&highByte,
					   						&lowByte);

	ASSERT_EQ( result, CONVERSION_ERROR_NEGATIVE_VALUE);
	PASS();
}


GREATEST_SUITE(FloatToPMBusUnsignedVOutLinearSuite)
{
    RUN_TEST(ZeroFloatToPMBusUnsignedVOutLinearShouldBeZero);
    RUN_TEST(ExceedingLargestPositiveFloatToPMBusUnsignedVOutLinearShouldSaturate);
    RUN_TEST(ExceedingSmallestPositiveFloatToPMBusUnsignedVOutLinearShouldSaturate);
 	RUN_TEST(NegativeFloatToPMBusUnsignedVOutLinearShouldReturnError);   
}
