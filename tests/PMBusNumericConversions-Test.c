// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "../vendor/greatest/greatest.h"

#include "ByteHelpers-TestSuite.c"

#include "PMBusLinearToFloat-TestSuite.c"
#include "FloatToPMBusLinear-TestSuite.c"

#include "PMBusUnsignedVOutLinearToFloat-TestSuite.c"
#include "FloatToPMBusUnsignedVOutLinear-TestSuite.c"

#include "PMBusSignedVOutLinearToFloat-TestSuite.c"
#include "FloatToPMBusSignedVOutLinear-TestSuite.c"

extern SUITE(PMBusLinearToFloatSuite);
extern SUITE(ByteHelpersSuite);

GREATEST_MAIN_DEFS(); //defs required for test runner main file

int main(int argc, char **argv)
{
	GREATEST_MAIN_BEGIN();

	RUN_SUITE(ByteHelpersSuite);

	RUN_SUITE(PMBusLinearToFloatSuite);
	RUN_SUITE(FloatToPMBusLinearSuite);
	RUN_SUITE(PMBusUnsignedVOutLinearToFloatSuite);
	RUN_SUITE(FloatToPMBusUnsignedVOutLinearSuite);
	RUN_SUITE(PMBusSignedVOutLinearToFloatSuite);
	RUN_SUITE(FloatToPMBusSignedVOutLinearSuite);
	
	
	GREATEST_MAIN_END();
}
