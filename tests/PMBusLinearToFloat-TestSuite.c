// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "../vendor/greatest/greatest.h"

#include "../src/DebugPrint.h"
#include "../src/ByteHelpers.h"
#include "../src/PMBusNumericConversions.h"

SUITE(PMBusLinearToFloatSuite);

TEST PMBusLinearToFloatZeroTest( void ) 
{
    unsigned short pmbusLinearZero = 0x0000;
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusLinearZero, &highByte, &lowByte);

    float convertedFloat = PMBusLinearToFloat( highByte, lowByte );
    ASSERT_EQ(0.0, convertedFloat);
    PASS();
}

TEST PMBusLinearToFloatAllSetBitsTest( void ) 
{
    // 5 bits of exponent of    
    // 0b11111 => -1 * (0b00000 + 1) => -1
    //
    // 11 bits of mantissa of
    // 0b11111111111 => -1 * (0b00...00 + 1) => -1 
    //
    // -1 * 2^-1 = decimal value of -0.5 
    unsigned short pmbusLinear = 0xFFFF;
    
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusLinear, &highByte, &lowByte);

    float convertedFloat = PMBusLinearToFloat( highByte, lowByte );
    DebugPrint("convertedfloat is %f\n", convertedFloat);
    ASSERT_EQ(-0.5, convertedFloat);
    PASS();
}

TEST PMBusLinearToFloatLargestNegativeValueTest( void ) 
{
    // 5 bits of exponent of    
    // 0b01111 => 15 
    //
    // 11 bits of mantissa of
    // 0b10000000000 => -1 * (0b01111111111 + 1) 
    //               => -1 * (0b10000000000)
    //               => -1024
    //
    // -1024 * 2^15  => decimal value of -33554432.0
    //
    // in hex:
    // 0b0111|1100|0000|0000 => 0x7C00
    //

    unsigned short pmbusLinear = 0x7C00;
    
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusLinear, &highByte, &lowByte);

    float convertedFloat = PMBusLinearToFloat( highByte, lowByte );
    
    DebugPrint("convertedfloat is %f\n", convertedFloat);
    ASSERT_EQ(-33554432.0, convertedFloat);
    PASS();
}

TEST PMBusLinearToFloatSmallestNegativeValueTest( void ) 
{
    // 5 bits of exponent of    
    // 0b10000 => -16 
    //
    // 11 bits of mantissa of
    // 0b11111111111 => -1 * (0b00...00 + 1) => -1 
    //
    // -1 * 2^-16  => decimal value of -1.5259e-5
    //
    // in hex:
    // 0b1000|0111|1111|1111 => 0x87FF
    //

    unsigned short pmbusLinear = 0x87FF;
    
    unsigned char highByte, lowByte;
    SplitShortToChars(pmbusLinear, &highByte, &lowByte);

    float expectedFloat = -1.0 * powf(2, -16);
//    float expectedFloat = -1.5259E-5; 
    float convertedFloat = PMBusLinearToFloat( highByte, lowByte );
    DebugPrint("convertedfloat is %f (in hex: %02x)\n", convertedFloat, *(unsigned int*)&convertedFloat );
    DebugPrint("expecting value of %f (in hex: %02x)\n", expectedFloat, *(unsigned int*)&expectedFloat);
    ASSERT_EQ(expectedFloat, convertedFloat);
    PASS();
}


GREATEST_SUITE(PMBusLinearToFloatSuite)
{
    RUN_TEST(PMBusLinearToFloatZeroTest);
    RUN_TEST(PMBusLinearToFloatAllSetBitsTest);
    RUN_TEST(PMBusLinearToFloatLargestNegativeValueTest);
    RUN_TEST(PMBusLinearToFloatSmallestNegativeValueTest);
}
