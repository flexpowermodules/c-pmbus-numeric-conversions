#!/bin/bash

echo "Running unit tests..."

#tests run in the present working directory, not script is in.
DIR="$( pwd )"
echo "Running tests in $DIR"

if [ "$OS" == "Windows_NT" ]; then
	TEST_EXEC=*-Test.exe
else
 	TEST_EXEC=*-Test
fi

for i in $TEST_EXEC
do
    if test -f $i
    then
    	./$i
    fi
done

echo "Done running unit tests."
