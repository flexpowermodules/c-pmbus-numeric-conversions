// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "../vendor/greatest/greatest.h"

#include "../src/DebugPrint.h"
#include "../src/PMBusNumericConversions.h"

SUITE(FloatToPMBusSignedVOutLinearSuite);

TEST ZeroFloatToPMBusSignedVOutLinearShouldBeZero( void )
{
	float zero = 0.0;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusSignedVOutLinear(zero,
											0x13,
					   						&highByte,
					   						&lowByte);

	ASSERT_EQ(highByte, 0x00);
	ASSERT_EQ( lowByte, 0x00);
	ASSERT_EQ( result, CONVERSION_OK);
	PASS();

} 

TEST ExceedingLargestPositiveFloatToPMBusSignedVOutLinearShouldSaturate( void ) 
{
	// highest possible for an exponent of -13 
	// 32767 * 2^(-13) => 3.99987792969 

	float exceedingLargestPositiveValue = 4.0; 
	signed char exponent = 0x13;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusSignedVOutLinear(exceedingLargestPositiveValue,
										exponent,
				   						&highByte,
				   						&lowByte);

	DebugPrint("highByte: %02X, lowByte: %02X, result: %d\n", highByte, lowByte, result);
	
	ASSERT_EQ(highByte, 0x7F);
	ASSERT_EQ( lowByte, 0xFF);
	ASSERT_EQ( result, CONVERSION_WARNING_SATURATED_LARGEST_POSITIVE_VALUE);
	PASS();
}

TEST ExceedingSmallestPositiveFloatToPMBusSignedVOutLinearShouldSaturate( void ) 
{
	// smallest possible for an exponent of -13
	//1 * 2^(-13) => 0.00012207031

	float exceedingSmallestPositiveValue = 0.00009; 
	signed char exponent = 0x13;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusSignedVOutLinear(exceedingSmallestPositiveValue,
											exponent,
					   						&highByte,
					   						&lowByte);


	DebugPrint("highByte: %02X, lowByte: %02X, result: %d\n", highByte, lowByte, result);
	
	ASSERT_EQ(highByte, 0x00);
	ASSERT_EQ( lowByte, 0x01);
	ASSERT_EQ( result, CONVERSION_WARNING_SATURATED_SMALLEST_POSITIVE_VALUE);
	PASS();


//TODO: another 'smallest' test where:
//
//  - the value is something smaller, like 0.00001, 
//  - currently this results in zero, but still reports back that the value saturated.
}

TEST ExceedingLargestNegativeFloatToPMBusSignedVOutLinearShouldSaturate( void ) 
{
	// largest negative possible for an exponent of -13 
	// -32768 * 2^(-13) => -4 

	float exceedingLargestPositiveValue = -4.01; 
	signed char exponent = 0x13;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusSignedVOutLinear(exceedingLargestPositiveValue,
										  exponent,
				   						  &highByte,
				   						  &lowByte);

	DebugPrint("highByte: %02X, lowByte: %02X, result: %d\n", highByte, lowByte, result);
	
	ASSERT_EQ(highByte, 0x80);
	ASSERT_EQ( lowByte, 0x00);
	ASSERT_EQ( result, CONVERSION_WARNING_SATURATED_LARGEST_NEGATIVE_VALUE);
	PASS();

}

TEST ExceedingSmallestNegativeFloatToPMBusSignedVOutLinearShouldSaturate( void ) 
{
	// smallest possible for an exponent of -13
	//-1 * 2^(-13) => 0.00012207031

	float exceedingSmallestPositiveValue = -0.00009; 
	signed char exponent = 0x13;

	unsigned char highByte; 
	unsigned char lowByte; 
	int result;

	result = FloatToPMBusSignedVOutLinear(exceedingSmallestPositiveValue,
											exponent,
					   						&highByte,
					   						&lowByte);


	DebugPrint("highByte: %02X, lowByte: %02X, result: %d\n", highByte, lowByte, result);
	
	ASSERT_EQ(highByte, 0xFF);
	ASSERT_EQ( lowByte, 0xFF);
	ASSERT_EQ( result, CONVERSION_WARNING_SATURATED_SMALLEST_NEGATIVE_VALUE);
	PASS();


	//TODO: another 'smallest' test where:
	//
	//  - the value is something smaller, like -0.00001, 
	//  - currently this results in zero, but still reports back that the value saturated.

}



GREATEST_SUITE(FloatToPMBusSignedVOutLinearSuite)
{
	RUN_TEST(ZeroFloatToPMBusSignedVOutLinearShouldBeZero);
	RUN_TEST(ExceedingLargestPositiveFloatToPMBusSignedVOutLinearShouldSaturate);
	RUN_TEST(ExceedingSmallestPositiveFloatToPMBusSignedVOutLinearShouldSaturate);
	RUN_TEST(ExceedingLargestNegativeFloatToPMBusSignedVOutLinearShouldSaturate);
	RUN_TEST(ExceedingSmallestNegativeFloatToPMBusSignedVOutLinearShouldSaturate);

}
