// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "../vendor/greatest/greatest.h"

#include "../src/DebugPrint.h"
#include "../src/PMBusNumericConversions.h"
// #include "PMBusNumericConversions.c" //for testing private methods


SUITE(FloatToPMBusLinearSuite);

TEST ZeroFloatToPMBusLinearShouldBeAllZeroes( void ) 
{

	float zero = 0.0;

	unsigned char pmbusLinearHighByte; 
	unsigned char pmbusLinearLowByte; 
	int result;

	result = FloatToPMBusLinear(zero,
					   &pmbusLinearHighByte,
					   &pmbusLinearLowByte);

	ASSERT_EQ(  pmbusLinearLowByte, 0x00);
	ASSERT_EQ( pmbusLinearHighByte, 0x00);
	PASS();

}

TEST ExceedingLargestPositiveFloatToPMBusLinearShouldSaturate( void ) 
{
	float exceedingLargestPositiveValue = 33521665.100000;

	//  NOTE: another test value such as 33521664.100000, 
	//  while written to be higher than the highest value by 0.1,
	//	will actually result in the same actual 
	//	float value as the constant LinearLargestPositiveValue

	unsigned char pmbusLinearHighByte; 
	unsigned char pmbusLinearLowByte; 
	int result;

	signed char  pmbusExponent;
	signed short pmbusMantissa;

	result = floatToPMBusLinearExponentAndMantissa(exceedingLargestPositiveValue, 
                                          &pmbusExponent, 
                                          &pmbusMantissa);

	DebugPrint("Calculated exponent is: %d, mantissa is: %d, result is: %d \n", 
			pmbusExponent, 
			pmbusMantissa,
			result);
	ASSERT_EQ( result, CONVERSION_WARNING_SATURATED_LARGEST_POSITIVE_VALUE);
	ASSERT_EQ( pmbusExponent, 15); 
	ASSERT_EQ( pmbusMantissa, 1023);

	result = FloatToPMBusLinear(exceedingLargestPositiveValue,
					   &pmbusLinearHighByte,
					   &pmbusLinearLowByte);

	//highest possible value for 1023 * 2^15
	//high byte will be: 01111|011 => 0x7B 
	// low byte will be: 11111111  => 0xFF
	DebugPrint("high byte is: %02X, low byte is: %02X \n", pmbusLinearLowByte, pmbusLinearHighByte );
	ASSERT_EQ(  pmbusLinearLowByte, 0xFF); 
	ASSERT_EQ( pmbusLinearHighByte, 0x7B);
	PASS();

}



TEST ExceedingLargestNegativeFloatToPMBusLinearShouldSaturate( void ) 
{
	float exceedingLargestNegativeValue = -33554434.100000;

	//  NOTE: another test value such as -33554434.000000, 
	//  while written to be lower than the lowest value by 2.1,
	//	will actually result in the same actual 
	//	float value as the constant LinearLargestNegativeValue

	unsigned char pmbusLinearHighByte; 
	unsigned char pmbusLinearLowByte; 
	int result;

	signed char  pmbusExponent;
	signed short pmbusMantissa;

	result = floatToPMBusLinearExponentAndMantissa(exceedingLargestNegativeValue, 
                                          &pmbusExponent, 
                                          &pmbusMantissa);

	DebugPrint("calculated exponent is: %d, mantissa is: %d, result is: %d \n", 
			pmbusExponent, 
			pmbusMantissa,
			result);
	ASSERT_EQ( result, CONVERSION_WARNING_SATURATED_LARGEST_NEGATIVE_VALUE);
	ASSERT_EQ( pmbusExponent, 15); 
	ASSERT_EQ( pmbusMantissa, -1024);

	result = FloatToPMBusLinear(exceedingLargestNegativeValue,
					   &pmbusLinearHighByte,
					   &pmbusLinearLowByte);

	//highest possible value for -1024 * 2^15
	//high byte will be: 01111|100 => 0x7C 
	// low byte will be: 00000000  => 0x00
	DebugPrint("high byte is: %02X, low byte is: %02X \n", pmbusLinearLowByte, pmbusLinearHighByte );
	ASSERT_EQ(  pmbusLinearLowByte, 0x00); 
	ASSERT_EQ( pmbusLinearHighByte, 0x7C);
	PASS();

}

TEST ThreePointThreeFloatToLinear( void ) 
{

	float testValue = 3.3;

	unsigned char pmbusLinearHighByte; 
	unsigned char pmbusLinearLowByte; 
	int result;

	signed char  pmbusExponent;
	signed short pmbusMantissa;

	result = floatToPMBusLinearExponentAndMantissa(testValue, 
                                          &pmbusExponent, 
                                          &pmbusMantissa);

	DebugPrint("calculated exponent is: %d, mantissa is: %d, result is: %d \n", 
			pmbusExponent, 
			pmbusMantissa,
			result);
	ASSERT_EQ( result, CONVERSION_OK);
	ASSERT_EQ( pmbusExponent, -8); 
	ASSERT_EQ( pmbusMantissa, 845);

	result = FloatToPMBusLinear(testValue,
					   &pmbusLinearHighByte,
					   &pmbusLinearLowByte);

	//highest possible value for 845 * 2^-8
	//high byte will be: 11000|011 => 0xC3 
	// low byte will be: 01001101  => 0x4D
	DebugPrint("high byte is: %02X, low byte is: %02X \n", pmbusLinearLowByte, pmbusLinearHighByte );
	ASSERT_EQ(  pmbusLinearLowByte, 0x4D); 
	ASSERT_EQ( pmbusLinearHighByte, 0xC3);
	PASS();
}


GREATEST_SUITE(FloatToPMBusLinearSuite)
{
    RUN_TEST(ZeroFloatToPMBusLinearShouldBeAllZeroes);
    RUN_TEST(ExceedingLargestPositiveFloatToPMBusLinearShouldSaturate);
	RUN_TEST(ExceedingLargestNegativeFloatToPMBusLinearShouldSaturate);
	RUN_TEST(ThreePointThreeFloatToLinear);
}
