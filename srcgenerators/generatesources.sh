#!/bin/bash

echo "Generating Sources..."

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"

shopt -s extglob

if [ "$OS" == "Windows_NT" ]; then
	GENERATOR_EXEC=*-Generator.exe
else
 	GENERATOR_EXEC=*-Generator
fi

for i in $GENERATOR_EXEC
do
	echo "Generating: $i"
    if test -f $i
    then
    	./$i
    fi
done

echo "moving generated source code to ../generatedsrc folder"

mkdir ../generatedsrc

for i in !(*-Generator.c|generatesources.sh)
do
	mv $i ../generatedsrc/$i
done

shopt -u extglob

echo "Done Generating & Moving Sources"
