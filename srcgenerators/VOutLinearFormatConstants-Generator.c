// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int powInt(int x, int p);
int VOutLinearExponentLargestPositiveValue( void );
int VOutLinearExponentLargestNegativeValue( void );
int UnsignedVOutLinearMantissaLargestPositiveValue( void );
int SignedVOutLinearMantissaLargestPositiveValue( void );
int SignedVOutLinearMantissaLargestNegativeValue( void );

int powInt(int x, int p)
{
  if (p == 0) return 1;
  if (p == 1) return x;

  int tmp = powInt(x, p/2);
  if (p%2 == 0) return tmp * tmp;
  else return x * tmp * tmp;
}

const int VOutLinearExponentBitCount = 5;
const int VOutLinearMantissaBitCount = 16;

int VOutLinearExponentLargestPositiveValue( void )
{
    return powInt(2, VOutLinearExponentBitCount - 1) - 1;   
}

int VOutLinearExponentLargestNegativeValue( void )
{
    return powInt(2, VOutLinearExponentBitCount - 1) * -1;
}

int UnsignedVOutLinearMantissaLargestPositiveValue( void )
{
    return powInt(2, VOutLinearMantissaBitCount ) - 1;   
}

int SignedVOutLinearMantissaLargestPositiveValue( void )
{
    return powInt(2, VOutLinearMantissaBitCount - 1 ) - 1;   
}

int SignedVOutLinearMantissaLargestNegativeValue( void )
{
    return powInt(2, VOutLinearMantissaBitCount - 1 ) * -1;   
}

int main(void) 
{
    char *cFilename = "VOutLinearFormatConstants.c";
    char *hFilename = "VOutLinearFormatConstants.h";

    FILE *hFile = fopen(hFilename, "w");
    FILE *cFile = fopen(cFilename, "w");
    
    time_t result = time(NULL);
    if (result == ((time_t)-1))
    {
       fprintf(stderr,"time() failed in file %s at line # %d\n", __FILE__,__LINE__-3);
       exit(EXIT_FAILURE);
    }

    //write c file
    printf("Writing to file %s\n", cFilename);

    fprintf(cFile, "//VOutLinearFormatConstants.h\n");
    fprintf(cFile, "//Generated on %s\n", asctime(localtime(&result)));
    fprintf(cFile, "\n");
    
    fprintf(cFile, "const int VOutLinearExponentBitCount = %d;\n", VOutLinearExponentBitCount);
    fprintf(cFile, "const int VOutLinearMantissaBitCount = %d;\n", VOutLinearMantissaBitCount);
    fprintf(cFile, "\n");

    fprintf(cFile, "const int VOutLinearExponentLargestPositiveValue = %d;\n", VOutLinearExponentLargestPositiveValue());
    fprintf(cFile, "const int VOutLinearExponentLargestNegativeValue = %d;\n", VOutLinearExponentLargestNegativeValue());
    fprintf(cFile, "\n");

    fprintf(cFile, "const int UnsignedVOutLinearMantissaLargestPositiveValue = %d;\n", UnsignedVOutLinearMantissaLargestPositiveValue());
    fprintf(cFile, "\n");

    fprintf(cFile, "const int SignedVOutLinearMantissaLargestPositiveValue = %d;\n", SignedVOutLinearMantissaLargestPositiveValue());
    fprintf(cFile, "const int SignedVOutLinearMantissaLargestNegativeValue = %d;\n", SignedVOutLinearMantissaLargestNegativeValue());
    fprintf(cFile, "\n");

    fprintf(cFile, "\n");

    //write h file
    printf("Writing to file %s\n", hFilename);
    
    fprintf(hFile, "//%s\n", hFilename);
    fprintf(hFile, "//Generated on %s\n", asctime(localtime(&result)));
    fprintf(hFile, "\n");

    fprintf(hFile, "extern const int VOutLinearExponentBitCount;\n");
    fprintf(hFile, "extern const int VOutLinearMantissaBitCount;\n");
    fprintf(hFile, "\n");

    fprintf(hFile, "extern const int VOutLinearExponentLargestPositiveValue;\n");
    fprintf(hFile, "extern const int VOutLinearExponentLargestNegativeValue;\n");
    fprintf(hFile, "\n");

    fprintf(hFile, "extern const int UnsignedVOutLinearMantissaLargestPositiveValue;\n");
    fprintf(hFile, "\n");

    fprintf(hFile, "extern const int SignedVOutLinearMantissaLargestPositiveValue;\n");
    fprintf(hFile, "extern const int SignedVOutLinearMantissaLargestNegativeValue;\n");
    fprintf(hFile, "\n");

    fclose(cFile);
	fclose(hFile);
}
