// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int powInt(int x, int p);
int LinearExponentLargestPositiveValue( void );
int LinearExponentLargestNegativeValue( void );
int LinearMantissaLargestPositiveValue( void );
int LinearMantissaLargestNegativeValue( void );
float LinearLargestNegativeValue( void );
float LinearLargestPositiveValue( void );
float LinearSmallestPositiveValue( void );
float LinearSmallestNegativeValue( void );

int powInt(int x, int p)
{
  if (p == 0) return 1;
  if (p == 1) return x;

  int tmp = powInt(x, p/2);
  if (p%2 == 0) return tmp * tmp;
  else return x * tmp * tmp;
}

const int LinearExponentBitCount = 5;
const int LinearMantissaBitCount = 11;

int LinearExponentLargestPositiveValue( void )
{
    return powInt(2, LinearExponentBitCount - 1) - 1;   
}

int LinearExponentLargestNegativeValue( void )
{
    return powInt(2, LinearExponentBitCount - 1) * -1;
}

int LinearMantissaLargestPositiveValue( void )
{
    return powInt(2, LinearMantissaBitCount - 1) - 1;   
} 

int LinearMantissaLargestNegativeValue( void )
{
    return powInt(2, LinearMantissaBitCount - 1) * -1;  
}

float LinearLargestNegativeValue( void )
{
    return (float) LinearMantissaLargestNegativeValue() * powf(2.0, (float) LinearExponentLargestPositiveValue());  
} 

float LinearLargestPositiveValue( void )
{
    return (float) LinearMantissaLargestPositiveValue() * powf(2.0, (float) LinearExponentLargestPositiveValue()); 
} 

float LinearSmallestPositiveValue( void )
{
    return 1.0 * powf(2.0, (float) LinearExponentLargestNegativeValue());  
} 

float LinearSmallestNegativeValue( void )
{
    return -1.0 * LinearSmallestPositiveValue();  
} 


int main (void)
{
    char *cFilename = "LinearFormatConstants.c";
    char *hFilename = "LinearFormatConstants.h";

    FILE *hFile = fopen(hFilename, "w");
    FILE *cFile = fopen(cFilename, "w");
    
    time_t result = time(NULL);
    if (result == ((time_t)-1))
    {
       fprintf(stderr,"time() failed in file %s at line # %d\n", __FILE__,__LINE__-3);
       exit(EXIT_FAILURE);
    }

    //write c file
    printf("Writing to file %s\n", cFilename);
    
    fprintf(cFile, "//%s\n", cFilename);
    fprintf(cFile, "//Generated on %s\n", asctime(localtime(&result)));
    fprintf(cFile, "\n");
    
    fprintf(cFile, "const int LinearExponentBitCount = %d;\n", LinearExponentBitCount);
    fprintf(cFile, "const int LinearMantissaBitCount = %d;\n", LinearMantissaBitCount);
    fprintf(cFile, "\n");

    fprintf(cFile, "const int LinearExponentLargestPositiveValue = %d;\n", LinearExponentLargestPositiveValue());
    fprintf(cFile, "const int LinearExponentLargestNegativeValue = %d;\n", LinearExponentLargestNegativeValue());
    fprintf(cFile, "\n");

    fprintf(cFile, "const int LinearMantissaLargestPositiveValue = %d;\n", LinearMantissaLargestPositiveValue());
    fprintf(cFile, "const int LinearMantissaLargestNegativeValue = %d;\n", LinearMantissaLargestNegativeValue());
    fprintf(cFile, "\n");

    fprintf(cFile, "const float LinearLargestNegativeValue = %f;\n", LinearLargestNegativeValue());
    fprintf(cFile, "const float LinearLargestPositiveValue = %f;\n", LinearLargestPositiveValue());
    fprintf(cFile, "\n");

    fprintf(cFile, "const float LinearSmallestNegativeValue = %f;\n", LinearSmallestNegativeValue());
    fprintf(cFile, "const float LinearSmallestPositiveValue = %f;\n", LinearSmallestPositiveValue());
    fprintf(cFile, "\n");

    fprintf(cFile, "\n");

    //write h file
    printf("Writing to file %s\n", hFilename);
    
    fprintf(hFile, "//%s\n", hFilename);
    fprintf(hFile, "//Generated on %s\n", asctime(localtime(&result)));
    fprintf(hFile, "\n");
    
    fprintf(hFile, "extern const int LinearExponentBitCount;\n");
    fprintf(hFile, "extern const int LinearMantissaBitCount;\n");
    fprintf(hFile, "\n");

    fprintf(hFile, "extern const int LinearExponentLargestPositiveValue;\n");
    fprintf(hFile, "extern const int LinearExponentLargestNegativeValue;\n");
    fprintf(hFile, "\n");

    fprintf(hFile, "extern const int LinearMantissaLargestPositiveValue;\n");
    fprintf(hFile, "extern const int LinearMantissaLargestNegativeValue;\n");
    fprintf(hFile, "\n");

    fprintf(hFile, "extern const float LinearLargestNegativeValue;\n");
    fprintf(hFile, "extern const float LinearLargestPositiveValue;\n");
    fprintf(hFile, "\n");

    fprintf(hFile, "extern const float LinearSmallestNegativeValue;\n");
    fprintf(hFile, "extern const float LinearSmallestPositiveValue;\n");
    fprintf(hFile, "\n");

    fprintf(hFile, "\n");



    fclose(cFile);
    fclose(hFile);
}
