// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#include <math.h>
#include "PMBusNumericConversions.h"
#include "../generatedsrc/LinearFormatConstants.h"
#include "../generatedsrc/VOutLinearFormatConstants.h"
#include "VOutLinearFormatBoundaryHelpers.h"
#include "ByteHelpers.h"

#ifdef DEBUG
#include <stdlib.h>
#include <stdio.h>

#include "DebugPrint.h"
#endif

//
// PMBus Linear conversions
// PMBus Linear is a floating point format with
// 5 bits of 2's complement exponent, and 11 bits of 2's complement mantissa
// together they are concatenated into a single 16-bit value.
//

float PMBusLinearToFloat ( unsigned char highByte, unsigned char lowByte )
{
    signed char exponent;

    //extract & sign-extend exponent
    exponent = highByte >> 3; 
    if(exponent & 0x10)
        exponent |= 0xE0;

    //sign-extend mantissa 
    if (highByte & 0x04)
        highByte |= 0xf8;
    else
        highByte &= 0x07;
    
    signed short mantissa = (((short)highByte) << 8) + (((short)lowByte) & 0x00FF);

    #ifdef DEBUG
        DebugPrint("mantissa is: 0x%02x (in decimal: %d) \n", mantissa, mantissa);
        DebugPrint("exponent is: 0x%02x (in decimal: %d) \n", exponent, exponent);
        DebugPrint("powf(2, exponent) is: %f \n", powf(2, exponent));
    #endif

    return mantissa * powf(2, exponent);
}

//returns status per the enum FloatToPMBusFloatStatus
int FloatToPMBusLinear (const float input, 
                        unsigned char * pmbusLinearHighByte, 
                        unsigned char * pmbusLinearLowByte )
{
    signed char pmbusExponent = 0;        
    signed short pmbusMantissa = 0;
    int result;

    result = floatToPMBusLinearExponentAndMantissa(input, 
                                                   &pmbusExponent,
                                                   &pmbusMantissa);

    formatPMBusLinearExponentAndMantissa(pmbusExponent, 
                                         pmbusMantissa,
                                         pmbusLinearHighByte, 
                                         pmbusLinearLowByte);

    return result;
}


//
// PMBus Linear helper methods
//
void formatPMBusLinearExponentAndMantissa(const signed char pmbusExponent,
                                          const signed short pmbusMantissa,
                                          unsigned char *pmbusLinearHighByte,
                                          unsigned char *pmbusLinearLowByte)
{
    *pmbusLinearHighByte  = 0xF8 & (pmbusExponent << 3);
    *pmbusLinearHighByte |= 0x07 & (char) (pmbusMantissa >> (LinearMantissaBitCount - 3));

    *pmbusLinearLowByte = (char) pmbusMantissa;     
}

int validateFloatAsPMBusLinearValue (const float input, 
                                     float *boundedOutput)
{
    
    if (input < LinearLargestNegativeValue)
    {
        *boundedOutput = LinearLargestNegativeValue;
        return CONVERSION_WARNING_SATURATED_LARGEST_NEGATIVE_VALUE;
    }
    if (input > LinearLargestPositiveValue)
    {
        *boundedOutput = LinearLargestPositiveValue;
        return CONVERSION_WARNING_SATURATED_LARGEST_POSITIVE_VALUE;
    }
    if (input < 0.0 && input > LinearSmallestNegativeValue)
    {
        *boundedOutput = LinearSmallestNegativeValue;
        return CONVERSION_WARNING_SATURATED_SMALLEST_NEGATIVE_VALUE;
    }
    if (input > 0.0 && input < LinearSmallestPositiveValue )
    {
        *boundedOutput = LinearSmallestPositiveValue;
        return CONVERSION_WARNING_SATURATED_SMALLEST_POSITIVE_VALUE;
    }
    
    
    *boundedOutput = input;
    return CONVERSION_OK;

}


int floatToPMBusLinearExponentAndMantissa(const float input, 
                                          signed char * pmbusExponent, 
                                          signed short * pmbusMantissa)
{
    int result;
    float boundedInput;
    float mantissaBeforeRounding;

    //
    // validate input
    //
    result = validateFloatAsPMBusLinearValue( input, 
                                              &boundedInput);
    if( result < CONVERSION_OK )
    {
        return result;
    }

    //
    // calculate exponent
    //
    if (input > 0.0)
    {
        *pmbusExponent = (signed char) ceilf( 
                               log10f( input / (float) LinearMantissaLargestPositiveValue) / 
                               log10f( 2.0 ) );
    }
    else if (input < 0.0)
    {
        *pmbusExponent = (signed char) ceilf( 
                               log10f( input / (float) LinearMantissaLargestNegativeValue) / 
                               log10f( 2.0 ) );
    }
    else 
        *pmbusExponent = 0;
    
    #ifdef DEBUG
      DebugPrint("exponent before saturation : %d\n", *pmbusExponent);
    #endif

    //saturate exponent to boundaries
    if (*pmbusExponent > LinearExponentLargestPositiveValue)
        *pmbusExponent = LinearExponentLargestPositiveValue;
    if (*pmbusExponent < LinearExponentLargestNegativeValue)
        *pmbusExponent = LinearExponentLargestNegativeValue;

    // 
    // calculate mantissa
    // 
    mantissaBeforeRounding = (input / powf(2.0, *pmbusExponent));
    //round result in direction of greater magnitude
    *pmbusMantissa = (signed short) ( (input >= 0.0) ? 
                        mantissaBeforeRounding + 0.5 : 
                        mantissaBeforeRounding - 0.5 );

    return result;
}


//
// PMBus Unsigned VOut Linear Conversions 
//
float PMBusUnsignedVOutLinearToFloat ( signed char voutLinearExponentOrVoutModeByte, 
                                       unsigned char highByte,
                                       unsigned char lowByte)
{
    unsigned short mantissa = (((short)highByte) << 8) + (((short)lowByte) & 0x00FF);

    return PMBusUnsignedVOutLinearToFloat_shortMantissa (voutLinearExponentOrVoutModeByte, mantissa);
}

float PMBusUnsignedVOutLinearToFloat_shortMantissa ( signed char voutLinearExponentOrVoutModeByte, 
                                                     unsigned short mantissa )
{
    signed char exponent = extractVoutLinearExponent( voutLinearExponentOrVoutModeByte );  
    return mantissa * powf(2.0, exponent);
}

//returns status per the enum FloatToPMBusFloatStatus
int FloatToPMBusUnsignedVOutLinear ( float positiveInput , 
                                     signed char voutLinearExponentOrVoutModeByte,
                                     unsigned char *highByte,
                                     unsigned char *lowByte)
{

    int result;
    unsigned short mantissa;
    result = FloatToPMBusUnsignedVOutLinear_shortMantissa ( positiveInput , 
                                                            voutLinearExponentOrVoutModeByte, 
                                                            &mantissa );

    SplitShortToChars(mantissa,
                      highByte,
                      lowByte);

    return result;
}

int FloatToPMBusUnsignedVOutLinear_shortMantissa ( float positiveInput , 
                                                   signed char voutLinearExponentOrVoutModeByte,
                                                   unsigned short * mantissa )
{

    int result;
    float boundedInput;
    float mantissaBeforeRounding;
    unsigned int mantissaBeforeTruncate;
    signed char exponent = extractVoutLinearExponent(voutLinearExponentOrVoutModeByte);
    
    //
    // validate input
    //
    result = validateFloatAsPMBusUnsignedVOutValue(positiveInput, 
                                                   exponent,
                                                   &boundedInput);
    if( result < CONVERSION_OK )
    {
        return result;
    }

    //
    // calculate mantissa
    //
    mantissaBeforeRounding = (positiveInput / powf(2.0, (float) exponent));  
    #ifdef DEBUG
        DebugPrint("mantissaBeforeRounding: %f \n", mantissaBeforeRounding);
    #endif
    mantissaBeforeTruncate = (unsigned int) (mantissaBeforeRounding + 0.5);
    

    //check for rounding causing overflow
    if ( mantissaBeforeTruncate > (unsigned int) UnsignedVOutLinearMantissaLargestPositiveValue )
        *mantissa = (unsigned int) UnsignedVOutLinearMantissaLargestPositiveValue;
    else
        *mantissa = (unsigned short) mantissaBeforeTruncate;

    return result;
}

int validateFloatAsPMBusUnsignedVOutValue (const float input,
                                           signed char voutLinearExponent,
                                           float *boundedOutput)
{
    float unsignedVOutLinearLargestPositiveValue;
    float unsignedVOutLinearSmallestPositiveValue;

    if (input < 0.0)
        return CONVERSION_ERROR_NEGATIVE_VALUE;

    unsignedVOutLinearLargestPositiveValue = UnsignedVOutLinearLargestPositiveValueForExponent(voutLinearExponent);
    #ifdef DEBUG
     DebugPrint("validation unsignedVOutLinearLargestPositiveValue: %f\n", unsignedVOutLinearLargestPositiveValue);
    #endif
    if (input > unsignedVOutLinearLargestPositiveValue)
    {
        *boundedOutput = unsignedVOutLinearLargestPositiveValue;
        return CONVERSION_WARNING_SATURATED_LARGEST_POSITIVE_VALUE;
    }

    unsignedVOutLinearSmallestPositiveValue = UnsignedVOutLinearSmallestPositiveValueForExponent(voutLinearExponent);
    #ifdef DEBUG
    DebugPrint("validation unsignedVOutLinearSmallestPositiveValue: %f\n", unsignedVOutLinearSmallestPositiveValue);
    #endif
    if (input > 0.0 && input < unsignedVOutLinearSmallestPositiveValue)
    {
        *boundedOutput = unsignedVOutLinearSmallestPositiveValue;
        return CONVERSION_WARNING_SATURATED_SMALLEST_POSITIVE_VALUE;
    }

    *boundedOutput = input;
    return CONVERSION_OK;
}



//
// PMBus Signed Vout Linear Conversions
// 

float PMBusSignedVOutLinearToFloat ( signed char voutLinearExponentOrVoutModeByte, 
                                                   unsigned char highByte,
                                                   unsigned char lowByte)
{
    signed short mantissa = (((short)highByte) << 8) + (((short)lowByte) & 0x00FF);
    return PMBusSignedVOutLinearToFloat_shortMantissa (voutLinearExponentOrVoutModeByte, mantissa);
}

float PMBusSignedVOutLinearToFloat_shortMantissa ( signed char voutLinearExponentOrVoutModeByte, 
                                                   signed short mantissa )
{
    signed char exponent = extractVoutLinearExponent( voutLinearExponentOrVoutModeByte );  
    return mantissa * powf(2.0, exponent);
}


int FloatToPMBusSignedVOutLinear( float input , 
                                    signed char voutLinearExponentOrVoutModeByte,
                                    unsigned char *highByte,
                                    unsigned char *lowByte )
{
    int result;
    signed short mantissa = 0;

    result = FloatToPMBusSignedVOutLinear_shortMantissa ( input , 
                                                            voutLinearExponentOrVoutModeByte, 
                                                            &mantissa );

    SplitShortToChars(mantissa,
                      highByte,
                      lowByte);

    return result;
}

int FloatToPMBusSignedVOutLinear_shortMantissa ( float input , 
                                   signed char voutLinearExponentOrVoutModeByte,
                                   signed short * mantissa )
{

    int result;
    float boundedInput;
    float mantissaBeforeRounding;
    signed int mantissaBeforeTruncate;
    signed char exponent = extractVoutLinearExponent(voutLinearExponentOrVoutModeByte);

    // 
    // validate input
    //
    result = validateFloatAsPMBusSignedVOutValue(input, 
                                                   exponent,
                                                   &boundedInput);
    if( result < CONVERSION_OK )
    {
        return result;
    }

    //
    // calculate mantissa
    //
    mantissaBeforeRounding = (input / powf(2.0, (float) exponent));  
    #ifdef DEBUG
    DebugPrint("mantissaBeforeRounding: %f \n ", mantissaBeforeRounding);
    #endif
    mantissaBeforeTruncate = (signed int) ( (input >= 0.0) ?
                              mantissaBeforeRounding + 0.5 :
                              mantissaBeforeRounding - 0.5 );

    //check for rounding overflow
    if ( mantissaBeforeTruncate > SignedVOutLinearMantissaLargestPositiveValue )
        *mantissa = SignedVOutLinearMantissaLargestPositiveValue;
    else if (mantissaBeforeTruncate < SignedVOutLinearMantissaLargestNegativeValue)
        *mantissa = SignedVOutLinearMantissaLargestNegativeValue;
    else
        *mantissa = (unsigned short) mantissaBeforeTruncate;

    return result;
}

int validateFloatAsPMBusSignedVOutValue (const float input,
                                         signed char voutLinearExponent,
                                         float *boundedOutput)
{

    float signedVOutLinearLargestPositiveValue;
    float signedVOutLinearSmallestPositiveValue;
    float signedVOutLinearLargestNegativeValue;
    float signedVOutLinearSmallestNegativeValue;

    signedVOutLinearLargestPositiveValue = SignedVOutLinearLargestPositiveValueForExponent(voutLinearExponent);
    if (input > signedVOutLinearLargestPositiveValue)
    {
        *boundedOutput = signedVOutLinearLargestPositiveValue;
        return CONVERSION_WARNING_SATURATED_LARGEST_POSITIVE_VALUE;
    }

    signedVOutLinearSmallestPositiveValue = SignedVOutLinearSmallestPositiveValueForExponent(voutLinearExponent);
    if (input > 0.0 && input < signedVOutLinearSmallestPositiveValue)
    {
        *boundedOutput = signedVOutLinearSmallestPositiveValue;
        return CONVERSION_WARNING_SATURATED_SMALLEST_POSITIVE_VALUE;
    }

    signedVOutLinearSmallestNegativeValue = SignedVOutLinearSmallestNegativeValueForExponent(voutLinearExponent);
    if (input < 0.0 && input > signedVOutLinearSmallestNegativeValue)
    {
        *boundedOutput = signedVOutLinearSmallestNegativeValue;
        return CONVERSION_WARNING_SATURATED_SMALLEST_NEGATIVE_VALUE;
    }

    signedVOutLinearLargestNegativeValue = SignedVOutLinearLargestNegativeValueForExponent(voutLinearExponent);
    if (input < signedVOutLinearLargestNegativeValue)
    {
        *boundedOutput = signedVOutLinearLargestNegativeValue;
        return CONVERSION_WARNING_SATURATED_LARGEST_NEGATIVE_VALUE;
    }

    *boundedOutput = input;
    return CONVERSION_OK;
}



signed char extractVoutLinearExponent(signed char voutLinearExponentOrVoutModeByte)
{
    //sign-extend exponent & remove upper bit data 
    return (voutLinearExponentOrVoutModeByte & 0x10) ? 
           (voutLinearExponentOrVoutModeByte | 0xE0) :
           (voutLinearExponentOrVoutModeByte & 0x1F) ;

}








