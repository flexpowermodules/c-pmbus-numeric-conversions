// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "VOutLinearFormatBoundaryHelpers.h"
#include "../generatedsrc/VOutLinearFormatConstants.h"

#include <math.h>

float UnsignedVOutLinearLargestPositiveValueForExponent(signed char exponent)
{
    return (float) UnsignedVOutLinearMantissaLargestPositiveValue * powf(2.0, (float) exponent); 
} 

float UnsignedVOutLinearSmallestPositiveValueForExponent(signed char exponent)
{
    return 1.0 * powf(2.0, (float) exponent);  
}

float SignedVOutLinearLargestPositiveValueForExponent(signed char exponent)
{
    return (float) SignedVOutLinearMantissaLargestPositiveValue * powf(2.0, (float) exponent); 
} 

float SignedVOutLinearSmallestPositiveValueForExponent(signed char exponent)
{
    return 1.0 * powf(2.0, (float) exponent);  
} 

float SignedVOutLinearSmallestNegativeValueForExponent(signed char exponent)
{
    return -1.0 * powf(2.0, (float) exponent);  
} 

float SignedVOutLinearLargestNegativeValueForExponent(signed char exponent)
{
    return (float) SignedVOutLinearMantissaLargestNegativeValue * powf(2.0, (float) exponent);  
} 
