//
// PMBus Numeric conversions
// 

// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef PMBUS_NUMERIC_CONVERSIONS_H
#define PMBUS_NUMERIC_CONVERSIONS_H

// PMBus Numeric Conversions Notes:
//
// 1 - float format assumes a 32-bit IEEE floating point number
// 2 - this file assumes a C99 compiler is used for the 
//     floating point math methods fabsf, powf, absf, etc.
//
//

// PMBus Linear Conversions
//
// The PMBus Linear format is a floating point format with
// 5 bits of 2's complement exponent, and 11 bits of 2's complement mantissa
// together they are concatenated into a single 16-bit value.
//
// More information in section 7.1 of the PMBus Specification Rev 1.2 Part II
//


// FloatToPMBusLinear returns status per the enum FloatToPMBusLinearStatus
float PMBusLinearToFloat ( unsigned char highByte, unsigned char lowByte );

int FloatToPMBusLinear (float inFloat, 
                        unsigned char * pmbusLinearHighByte, 
                        unsigned char * pmbusLinearLowByte);


//
// PMBus Unsigned Vout Linear Conversions
// this is typically used for the VOUT_TRIM & VOUT_CAL_OFFSET commands
// as the value may have a negative value as mentioned in the 
// PMBus specification 1.2. 
float PMBusUnsignedVOutLinearToFloat ( signed char voutLinear, 
                                       unsigned char highByte,
                                       unsigned char lowByte);

float PMBusUnsignedVOutLinearToFloat_shortMantissa ( signed char voutLinearExponent, 
                                                     unsigned short mantissa );

int FloatToPMBusUnsignedVOutLinear ( float positiveInput , 
                                     signed char voutLinearExponentOrVoutModeByte,
                                     unsigned char *highByte,
                                     unsigned char *lowByte);

int FloatToPMBusUnsignedVOutLinear_shortMantissa ( float positiveInput , 
                                                   signed char voutLinearExponentOrVoutModeByte,
                                                   unsigned short * mantissa );


//
// PMBus Signed Vout Linear Conversions
// this is typically used for the VOUT_TRIM & VOUT_CAL_OFFSET commands
// as the value may have a negative value as mentioned in the 
// PMBus specification 1.2. 
// 
float PMBusSignedVOutLinearToFloat ( signed char voutLinearExponentOrVoutModeByte, 
                                                   unsigned char highByte,
                                                   unsigned char lowByte );

float PMBusSignedVOutLinearToFloat_shortMantissa ( signed char voutLinearExponentOrVoutModeByte, 
                                                   signed short mantissa );

int FloatToPMBusSignedVOutLinear( float input , 
                                    signed char voutLinearExponentOrVoutModeByte,
                                    unsigned char *highByte,
                                    unsigned char *lowByte );

int FloatToPMBusSignedVOutLinear_shortMantissa ( float input , 
                                   signed char voutLinearExponentOrVoutModeByte,
                                   signed short * mantissa );


//
// helper methods
//
void formatPMBusLinearExponentAndMantissa(const signed char pmbusExponent,
                                          const signed short pmbusMantissa,
                                          unsigned char *pmbusLinearHighByte,
                                          unsigned char *pmbusLinearLowByte);

int floatToPMBusLinearExponentAndMantissa(const float input, 
                                          signed char * pmbusExponent, 
                                          signed short * pmbusMantissa);

int validateFloatAsPMBusLinearValue(const float input, 
                                    float *boundedOutput);

int validateFloatAsPMBusUnsignedVOutValue (const float input,
                                           signed char voutLinearExponent,
                                           float *boundedOutput);

int validateFloatAsPMBusSignedVOutValue (const float input,
                                         signed char voutLinearExponent,
                                         float *boundedOutput);

signed char extractVoutLinearExponent(signed char voutLinearExponentOrVoutModeByte);



enum FloatToPMBusFloatStatus
{
  	CONVERSION_WARNING_SATURATED_SMALLEST_NEGATIVE_VALUE = 4,
  	CONVERSION_WARNING_SATURATED_SMALLEST_POSITIVE_VALUE = 3,
  	CONVERSION_WARNING_SATURATED_LARGEST_POSITIVE_VALUE = 2,
  	CONVERSION_WARNING_SATURATED_LARGEST_NEGATIVE_VALUE = 1,
    CONVERSION_OK = 0,
    CONVERSION_ERROR_UNKNOWN = -1,
    CONVERSION_ERROR_INVALID_INPUT = -2,
    CONVERSION_ERROR_NEGATIVE_VALUE = -3
};

#endif
